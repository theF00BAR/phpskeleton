<?php

/**
 * This file returns the application routes.
 * This file overrides the /skeleton/config/routes.php file.
 */

declare(strict_types=1);

//By default this file contains example routes that can be deleted.

return array_merge(require APP_ROOT . 'skeleton/config/routes.php', [
]);
