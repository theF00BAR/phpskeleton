<?php

declare(strict_types=1);

namespace AppSkeleton\Environment;

use Throwable;

use function
    is_string,
    json_decode,
    strlen,
    substr,
    array_key_exists,
    getenv;

/**
 * Reads environment variables.
 */
final class VarReader
{

    /**
     * Reads an environment variable.
     *
     * @param string $name
     * @param null $default
     *
     * @return mixed
     */
    public static function read(string $name, $default = null)
    {

        $value = self::getRawValue($name);

        if (null === $value) return $default;
        if (!self::isSpecialValue($value)) return $value;

        /** @noinspection PhpStrictTypeCheckingInspection */
        return self::parseSpecialValue($value); //isSpecialValue ensures string.

    }

    /**
     * @param string $sValue
     *
     * @return mixed|null
     */
    private static function parseSpecialValue(string $sValue)
    {

        //Assume that $sValue has the (<value>) or ((<value) formats.

        //Check for escaped special value.
        if ($sValue[0] === '(' && $sValue[1] === '(')
            return substr($sValue, 1);

        $json = substr($sValue, 1, strlen($sValue) - 2);

        try {

            $json = json_decode(
                $json, false, 16, JSON_THROW_ON_ERROR);

            return $json;

        } catch (Throwable $e) {
        }

        return $sValue;

    }

    /**
     * @param mixed $value
     *
     * @return bool
     */
    private static function isSpecialValue($value): bool
    {

        if (is_string($value)) {
            $len = strlen($value);

            if ($len >= 3)
                return $value[0] === '(' && $value[$len - 1] === ')';

        }

        return false;

    }

    /**
     * @param string $name
     *
     * @return mixed|null
     */
    private static function getRawValue(string $name)
    {

        if (array_key_exists($name, $_ENV)) {
            if (null === $_ENV[$name]) return '(null)';
            return $_ENV[$name];
        }

        if (array_key_exists($name, $_SERVER)) {
            if (null === $_SERVER[$name]) return '(null)';
            return $_SERVER[$name];
        }

        $x = getenv($name);
        if ($x === false) return null;

        return $x;

    }

    private function __construct()
    {
    }

}
