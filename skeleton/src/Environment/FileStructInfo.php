<?php

declare(strict_types=1);

namespace AppSkeleton\Environment;

use AppSkeleton\Utilities\IoUtilities;

/**
 * Provides utility methods to get information about the application
 * filesystem structure.
 */
final class FileStructInfo
{

    private const APP_ROOT = __DIR__ . '/../../..';

    /**
     * Gets the full filename for an application file or directory.
     *
     * @param string $filename
     *
     * @return string
     */
    public static function filename(string $filename): string
    {
        return IoUtilities::joinPaths(self::APP_ROOT, $filename);
    }

    private function __construct()
    {
    }

}
