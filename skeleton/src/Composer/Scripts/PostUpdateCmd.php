<?php

declare(strict_types=1);

namespace AppSkeleton\Composer\Scripts;

use AppSkeleton\Environment\FileStructInfo;

use function
    is_file,
    file_exists,
    copy,
    count,
    file_put_contents;

/**
 * Composer script for the "post-update-cmd" event.
 */
final class PostUpdateCmd
{

    public static function exec(): void
    {
        self::secureVendorDirectory();
    }

    private static function secureVendorDirectory(): void
    {

        $copySrc = [
            FileStructInfo::filename('/skeleton/.htaccess'),
            FileStructInfo::filename('/skeleton/index.php'),
            FileStructInfo::filename('/skeleton/index.html')
        ];

        $copyDst = [
            FileStructInfo::filename('/vendor/.htaccess'),
            FileStructInfo::filename('/vendor/index.php'),
            FileStructInfo::filename('/vendor/index.html')
        ];

        for ($i = 0; $i < count($copySrc); $i++) {
            if (is_file($copySrc[$i]) && file_exists($copySrc[$i]))
                @copy($copySrc[$i], $copyDst[$i]);
        }

        @file_put_contents(
            FileStructInfo::filename('/vendor/.gitignore'),
            "*\n");

    }

    private function __construct()
    {
        //Disabled.
    }

}
