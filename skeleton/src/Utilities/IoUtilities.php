<?php

declare(strict_types=1);

namespace AppSkeleton\Utilities;

use function
    trim,
    strlen,
    substr;

/**
 * Provides utilities for file paths and IO operations.
 */
final class IoUtilities
{

    /**
     * Concatenate paths into a single path.
     *
     * @param string $left
     * @param string $right
     *
     * @return string
     */
    public static function joinPaths(string $left, string $right): string
    {

        $left = trim($left);
        $right = trim($right);
        $lLen = strlen($left);
        $rLen = strlen($right);

        //Return only one of the paths if the other one is empty.
        if ($lLen === 0) return $right;
        if ($rLen === 0) return $left;

        //Join the two paths removing or adding necessary separators.
        $lTrailing = $left[$lLen - 1] === '/';
        $rLeading = $right[0] === '/';

        if ($lTrailing && $rLeading)
            return $left . substr($right, 1);

        if (!$lTrailing && !$rLeading)
            return $left . '/' . $right;

        return $left . $right;

    }

    private function __construct()
    {
    }

}
