<?php

/**
 * This file declares common functions required
 * for the application to run.
 */

declare(strict_types=1);

use AppSkeleton\Environment\VarReader;

/**
 * Gets an environment variable.
 *
 * @param string $name
 * @param null $default
 *
 * @return mixed
 */
function env_(string $name, $default = null)
{
    return VarReader::read($name, $default);
}
