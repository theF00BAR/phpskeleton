<?php

/**
 * This file declares common constants required
 * for the application to run.
 */

declare(strict_types=1);

/**
 * @var string APP_ROOT Application root directory with a trailing slash.
 */
define('APP_ROOT', __DIR__ . '/../../../');
