<?php

/**
 * This file returns the default application routes.
 * The default application routes can be overwritten by the /config/routes.php file.
 */

declare(strict_types=1);

return [
];
