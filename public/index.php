<?php

declare(strict_types=1);

use Dotenv\Dotenv;

//Include Composer autoloader.
require_once __DIR__ . '/../vendor/autoload.php';

//Load environment variables.
Dotenv::create(__DIR__ . '/../')->safeLoad();
