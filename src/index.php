<?php

declare(strict_types=1);

$protocol = isset($_SERVER['SERVER_PROTOCOL'])
    ? $_SERVER['SERVER_PROTOCOL']
    : 'HTTP/1.1';

header("$protocol 301 Moved Permanently", true, 301);
header('Location: ../', true, 301);
